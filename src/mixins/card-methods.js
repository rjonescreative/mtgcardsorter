import ApiService from '../services/api-services';

export default {
  mixins: [
    ApiService,
  ],
  data() {
    return {
      maxListSize: 75,
      apiDelay: 1000,
      waitToCall: false,
      snackbar: false,
      message: '',
    };
  },
  methods: {
    clearIsActive() {
      document.querySelectorAll('.card').forEach((el) => {
        el.classList.remove('isActive');
      });
    },
    getNamedCard(cardName) {
      const query = `?fuzzy=${encodeURI(cardName)}`;
      return new Promise((resolve, reject) => {
        this.waitToCall = true;
        ApiService.getNamedCard(query)
          .then((res) => {
            setTimeout(() => {
              this.waitToCall = false;
            }, this.apiDelay);
            if (res.object === 'error') {
              this.message = `Error: ${res.details}`;
              this.snackbar = true;
              reject(res);
            }
            resolve(res);
          })
          .catch((err) => {
            // eslint-disable-next-line
            console.log('catch', err);
            reject(err);
          });
      });
    },
    getCardCollection(collection) {
      return new Promise((resolve, reject) => {
        ApiService.getCardCollection(collection)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            // eslint-disable-next-line
            console.log(err);
            reject(err);
          });
      });
    },
    getURI(uri) {
      return new Promise((resolve, reject) => {
        this.waitToCall = true;
        ApiService.getURI(uri)
          .then((res) => {
            setTimeout(() => {
              this.waitToCall = false;
            }, this.apiDelay);
            if (res.object === 'error') {
              this.message = `Error: ${res.details}`;
              this.snackbar = true;
              reject(res);
            }
            resolve(res);
          })
          .catch((err) => {
            // eslint-disable-next-line
            console.log('catch', err);
            reject(err);
          });
      });
    },
  },
};

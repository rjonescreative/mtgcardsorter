import Vue from 'vue';
import Router from 'vue-router';
import CardSorter from '@/components/CardSorter';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'CardSorter',
      component: CardSorter,
    },
  ],
});
